import React from "react";
import Lista from "./Lista";

import socketIOClient from 'socket.io-client';
import sailsIOClient from  'sails.io.js'

var io = sailsIOClient(socketIOClient);
//io.sails.url = 'http://10.0.0.177:1337';
//io.sails.url = 'http://10.0.2.178:1337';
io.sails.url = 'http://192.168.1.103:1337';



class App extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        comentarios: [],
        text: '',
        id_comentario: '',
      };

      this.inserirNoBancoeNoEstado = this.inserirNoBancoeNoEstado.bind(this);
      this.handleTextChange = this.handleTextChange.bind(this);

    }
  
    escutarEventos(){
        console.log(' olhando....fora');
        io.socket.on('comentario', (event) => {
            if(event.verb === 'created') {
              console.log(' olhando....',event.data.id_comentario);
              this.setState({
                comentarios: [
                  ...this.state.comentarios,
                  {text: event.data.text, id_comentario:event.data.id_comentario}
                ]
              })
            } else {
              console.log('nah');
            }
        });
    }

    preencherState()
    {
        io.socket.get('/comentario/listar',(resData, jwres) => {
            console.log(' Posts========>',resData);
            resData.map(comentario => (
                this.setState({
                    comentarios: [
                      ...this.state.comentarios,
                      {text: comentario.text, id_comentario: comentario.id_comentario}
                    ]
                  })
            ))
          })
    }

    componentDidMount() {
        this.escutarEventos();
        this.preencherState();
    }

    inserirNoBancoeNoEstado(e) {
        var text = this.state.text;
        var id_comentario = Math.random();
        console.log('id_comentario de cadastro...', id_comentario);      
        io.socket.post('/comentario/adicionar', {
            text,
            id_comentario
        },function (resData, jwRes) {
            console.log('persistindo...', resData);
            console.log('resultado...', jwRes);        
        });
        this.setState({
            comentarios: [
              ...this.state.comentarios,
              {text: this.state.text, id_comentario}
            ]
          })
        this.setState({ text: "" });
        e.preventDefault();
    }
  
    handleTextChange(e) {
      this.setState({ text: e.target.value });
    }
  


    render() {

      return ( 
              <div>
                      <form onSubmit={this.inserirNoBancoeNoEstado}>
                        <input
                          value={this.state.text}
                          onChange={this.handleTextChange}
                        />
                        <button type="submit">Comentar</button>
                      </form> 
                      { 
                         this.state.comentarios.map((comentario, i) => (
                            <Lista title={comentario.text} id_comentario={comentario.id_comentario} key={i}/>
                        ))
                      }
                     
              </div>
      );
    }
  }
  

export default App;